package controle;

import entidade.Calculadora;
import java.util.Scanner;

public class Programa {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Calculadora c1 = new Calculadora();

        System.out.println("Informe o valor 1: ");
        double valor1 = sc.nextDouble();

        System.out.println("Informe o valor 2: ");
        double valor2 = sc.nextDouble();

        System.out.println("Soma: " + c1.soma(valor1, valor2));
        System.out.println("Subtração: " + c1.subtracao(valor1, valor2));
        System.out.println("Multiplicação: " + c1.multiplicacao(valor1, valor2));
        System.out.println("Divisão sem formatação: " + c1.divisao(valor1, valor2));
        System.out.printf("Divisão com formatação: %.2f", c1.divisao(valor1, valor2));

        sc.close();

    }

}
