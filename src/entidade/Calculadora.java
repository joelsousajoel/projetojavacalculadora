package entidade;

public class Calculadora {

    public double soma(double valor1, double valor2) {
        double resultado = valor1 + valor2;
        return resultado;
    }

    public double subtracao(double valor1, double valor2) {
        double resultado = valor1 - valor2;
        return resultado;
    }

    public double multiplicacao(double valor1, double valor2) {
        double resultado = valor1 * valor2;
        return resultado;
    }

    public double divisao(double valor1, double valor2) {
        double resultado = valor1 / valor2;
        return resultado;
    }

}
